import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
// the AS keyword gives an alias to the component
import './App.css';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Error from './pages/Error';
import Calculator from './pages/Calculator';

/*
  All other components/pages will be contained in our main component: <App />

  <> .. </> - Fragment which ensures that adjacent JSX elements will be rendered and avoid this error.
*/
function App() {
  return (
  
    <Router>
      <>
      <AppNavBar/>
      <Container >
        <Switch>
          <Route exact path="/"component={Home}/>
          <Route exact path="/courses"component={Courses}/>
          <Route exact path="/register"component={Register}/>
          <Route exact path="/login"component={Login}/>
          <Route exact path="/calculator"component={Calculator}/>
          <Route component={Error}/>
        </Switch>
      </Container>
    </>
    </Router>

  );
}

export default App;