// Long Method
// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';

// Short Method
import { Container, Nav, Navbar } from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom';

export default function AppNavBar(){
	return(
		<Navbar bg="light" expand="lg">
		      <Container>
		        <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		        {/*
		        	mr -> me
					ml -> ms
		        */}
		          <Nav className="ms-auto">
		            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
		            <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
		            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
		            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
		            <Nav.Link as={NavLink} to="/calculator">Calculator</Nav.Link>
		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>
	);
}